FROM maven:3.8.1-openjdk-17 AS build
COPY . .
RUN mvn -f /pom.xml clean package

FROM openjdk:17
COPY --from=build /target/*.jar app.jar
RUN chown -R 1000:1000
USER 1000:1000
ENTRYPOINT ["java", "-jar", "/app.jar"]