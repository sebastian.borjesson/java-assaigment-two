# Dependencies/Libraries/Tech:
- JDK 17
- Spring Web
- JDBC API
- thymeleaf
- Sqlite
- Postman API https://www.getpostman.com/collections/d842a0c6b24bd78614fb

# Structure:
- Repository pattern split into two folder: Customer and Music.
- Services are named RepositoryImpl and are placed inside Repository folder.

# Features:
- Customer CRUD without D
- Search for music tracks

# Deployment:
- Build with maven in intelliJ
- Or build with docker

# Version:
- 0.0.1-SNAPSHOT

# Developers:
- Sebastian Börjesson @sebastian.borjesson
- Christopher Vestman @druwan
- Nils Jacobsen @nils_jacobsen
