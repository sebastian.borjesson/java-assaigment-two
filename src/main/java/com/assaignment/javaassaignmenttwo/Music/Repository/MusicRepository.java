package com.assaignment.javaassaignmenttwo.Music.Repository;

import com.assaignment.javaassaignmenttwo.Music.Models.MusicArtist;
import com.assaignment.javaassaignmenttwo.Music.Models.MusicGenre;
import com.assaignment.javaassaignmenttwo.Music.Models.MusicSearch;
import com.assaignment.javaassaignmenttwo.Music.Models.MusicTrack;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

//Methods are linked to endpoints in Music/MusicController.

@Repository
public interface MusicRepository {

    ArrayList<MusicGenre> getFiveRandomGenres();
    ArrayList<MusicArtist> getFiveRandomArtists();
    ArrayList<MusicTrack> getFiveRandomTracks();
    ArrayList<MusicSearch> getTracksByTitleSearch(String title);

}
