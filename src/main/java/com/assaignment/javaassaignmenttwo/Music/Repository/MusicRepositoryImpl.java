package com.assaignment.javaassaignmenttwo.Music.Repository;

import com.assaignment.javaassaignmenttwo.Customer.Models.Customer;
import com.assaignment.javaassaignmenttwo.Customer.Repository.ConnectionSetup;
import com.assaignment.javaassaignmenttwo.Music.Models.MusicArtist;
import com.assaignment.javaassaignmenttwo.Music.Models.MusicGenre;
import com.assaignment.javaassaignmenttwo.Music.Models.MusicSearch;
import com.assaignment.javaassaignmenttwo.Music.Models.MusicTrack;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

//Implements the MusicRepository Interface.
@Service
public class MusicRepositoryImpl implements MusicRepository{

    public ArrayList<MusicGenre> getFiveRandomGenres() {
        ArrayList<MusicGenre> musicGenres = new ArrayList<>();
        try (Connection conn = ConnectionSetup.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Name FROM Genre ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                musicGenres.add(
                        new MusicGenre(
                                resultSet.getString("Name")
                        ));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return musicGenres;
    }

    public ArrayList<MusicArtist> getFiveRandomArtists() {
        ArrayList<MusicArtist> musicArtists = new ArrayList<>();
        try (Connection conn = ConnectionSetup.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Name FROM Artist ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                musicArtists.add(
                        new MusicArtist(
                                resultSet.getString("Name")
                        ));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return musicArtists;
    }

    public ArrayList<MusicTrack> getFiveRandomTracks() {
        ArrayList<MusicTrack> musicTracks = new ArrayList<>();
        try (Connection conn = ConnectionSetup.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Name FROM Track ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                musicTracks.add(
                        new MusicTrack(
                                resultSet.getString("Name")
                        ));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return musicTracks;
    }

    public ArrayList<MusicSearch> getTracksByTitleSearch(String title) {
        ArrayList<MusicSearch> musicSearches = new ArrayList<>();
        try (Connection conn = ConnectionSetup.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Track.Name AS Track, Artist.Name AS Artist, Album.Title AS Album, Genre.Name AS Genre FROM Track INNER JOIN Album ON Track.AlbumId = Album.AlbumId INNER JOIN Genre ON Track.GenreId = Genre.GenreId INNER JOIN Artist ON Album.ArtistId = Artist.ArtistId WHERE Track.Name LIKE ? ORDER BY Track.Name");
            preparedStatement.setString(1, '%' + title + '%');

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                musicSearches.add(
                        new MusicSearch(
                                resultSet.getString("Track"),
                                resultSet.getString("Artist"),
                                resultSet.getString("Album"),
                                resultSet.getString("Genre")
                        ));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return musicSearches;
    }
}
