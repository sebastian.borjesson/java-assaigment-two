package com.assaignment.javaassaignmenttwo.Music.Controllers;

import com.assaignment.javaassaignmenttwo.Music.Repository.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

//Controller for thymeleaf in resources/templates home.html and search.html

@Controller
public class MusicController {

    private final MusicRepository musicRepository;

    public MusicController(@Autowired MusicRepository musicRepository) {
        this.musicRepository = musicRepository;
    }

    //Uses MusicArtist, MusicGenre and MusicTrack models in Music/Models.
    //Provides five randomized values from the database for each model with every reload.
    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("Tracks" , musicRepository.getFiveRandomTracks());
        model.addAttribute("Genres", musicRepository.getFiveRandomGenres());
        model.addAttribute("Artists", musicRepository.getFiveRandomArtists());
        return "home";
    }

    //Uses MusicSearch model in Music/Models
    //Will not work with empty input.
    //Return results containing parts of the track title based on the input provided.
    @GetMapping("/search")
    public String search(@ModelAttribute(value = "name") String name, Model model) {
        if (name != null) {
            model.addAttribute("Search",  musicRepository.getTracksByTitleSearch(name));
        }
        return "search";
    }
}
