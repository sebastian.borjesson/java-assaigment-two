package com.assaignment.javaassaignmenttwo.Music.Models;

public class MusicArtist {

    String name;

    public MusicArtist(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
