package com.assaignment.javaassaignmenttwo.Music.Models;

public class MusicTrack {

    String name;

    public MusicTrack(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
