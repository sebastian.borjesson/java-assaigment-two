package com.assaignment.javaassaignmenttwo.Music.Models;

public class MusicGenre {

    String name;

    public MusicGenre(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

}
