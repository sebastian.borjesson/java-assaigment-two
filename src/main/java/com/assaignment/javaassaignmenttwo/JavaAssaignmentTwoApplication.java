package com.assaignment.javaassaignmenttwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaAssaignmentTwoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaAssaignmentTwoApplication.class, args);
    }

}
