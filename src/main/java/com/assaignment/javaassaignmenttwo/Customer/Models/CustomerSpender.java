package com.assaignment.javaassaignmenttwo.Customer.Models;

public class CustomerSpender {
     int customerId;
     String firstName;
     String lastName;
     double invoiceTotal;

    public CustomerSpender(int customerId, String firstName, String lastName, double invoiceTotal) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.invoiceTotal = invoiceTotal;
    }

    public int getCustomerId() {
        return customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getInvoiceTotal() {
        return invoiceTotal;
    }
}
