package com.assaignment.javaassaignmenttwo.Customer.Models;

public class CustomerCountry {
    String country;
    int totalCustomers;

    public CustomerCountry(String country, int totalCustomers) {
        this.country = country;
        this.totalCustomers = totalCustomers;
    }

    public String getCountry() {
        return country;
    }

    public int getTotalCustomers() {
        return totalCustomers;
    }
}
