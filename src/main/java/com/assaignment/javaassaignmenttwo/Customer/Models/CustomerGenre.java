package com.assaignment.javaassaignmenttwo.Customer.Models;

public class CustomerGenre {
    String genreName;
    int genreValue;

    public CustomerGenre(String genreName, int genreValue) {
        this.genreName = genreName;
        this.genreValue = genreValue;
    }

    public String getGenreName() {
        return genreName;
    }

    public int getGenreValue() {
        return genreValue;
    }
}
