package com.assaignment.javaassaignmenttwo.Customer.Models;

public class Customer {
    int customerId;
    String firstName;
    String lastName;
    String country;
    String postalCode;
    String phone;
    String email;

    public Customer(int CustomerId, String FirstName, String LastName, String Country, String PostalCode, String Phone, String Email) {
        this.customerId = CustomerId;
        this.firstName = FirstName;
        this.lastName = LastName;
        this.country = Country;
        this.postalCode = PostalCode;
        this.phone = Phone;
        this.email = Email;
    }

    public int getCustomerId() {
        return customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCountry() { return country; }

    public String getPostalCode() { return postalCode; }

    public String getPhone() { return phone; }

    public String getEmail() { return email; }
}
