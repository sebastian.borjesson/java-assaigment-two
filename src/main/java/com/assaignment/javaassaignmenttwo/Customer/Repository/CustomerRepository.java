package com.assaignment.javaassaignmenttwo.Customer.Repository;

import com.assaignment.javaassaignmenttwo.Customer.Models.Customer;
import com.assaignment.javaassaignmenttwo.Customer.Models.CustomerCountry;
import com.assaignment.javaassaignmenttwo.Customer.Models.CustomerGenre;
import com.assaignment.javaassaignmenttwo.Customer.Models.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

//Methods are linked to endpoints in Controllers/CustomerController.

@Repository
public interface CustomerRepository {
     ArrayList<Customer> getAllCustomers();
     Customer getCustomerById(int customerId);
     ArrayList<Customer> getCustomersByName(String name);
     boolean addCustomer(Customer customer);
     boolean updateCustomer(Customer customer, int id);
     ArrayList<CustomerCountry> getAmountOfCustomersInEachCountry();
     ArrayList<Customer> getCustomerPage(int offset);
     ArrayList<CustomerSpender> getTotalSpent();
     ArrayList<CustomerGenre> getMostPopularGenreByCustomer(int id);
}
