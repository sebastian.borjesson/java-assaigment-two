package com.assaignment.javaassaignmenttwo.Customer.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//In each of the methods in CustomerRepositoryImpl and MusicRepositoryImpl, -
//Connection conn = ConnectionSetup.getConnection() is passed as a parameter in the try/catch.
//This opens the connection to the database which is automatically closed at the end of the method.
public class ConnectionSetup {
    static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";

    static public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL);
    }
}
