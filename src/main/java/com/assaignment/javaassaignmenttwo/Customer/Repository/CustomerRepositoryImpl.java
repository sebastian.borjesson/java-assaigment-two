package com.assaignment.javaassaignmenttwo.Customer.Repository;

import com.assaignment.javaassaignmenttwo.Customer.Models.Customer;
import com.assaignment.javaassaignmenttwo.Customer.Models.CustomerCountry;
import com.assaignment.javaassaignmenttwo.Customer.Models.CustomerGenre;
import com.assaignment.javaassaignmenttwo.Customer.Models.CustomerSpender;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.*;

//This class implements the CustomerRepository Interface.

@Service
public class CustomerRepositoryImpl implements CustomerRepository {

    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();
        try (Connection conn = ConnectionSetup.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customers.add(
                    new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                ));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return customers;
    }


    public Customer getCustomerById(int customerId) {

        Customer customer = null;
        try (Connection conn = ConnectionSetup.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = ?");
            preparedStatement.setInt(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return customer;
    }


    public ArrayList<Customer> getCustomersByName(String name) {
        ArrayList<Customer> customers = new ArrayList<>();
        try (Connection conn = ConnectionSetup.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE ?");
            preparedStatement.setString(1, name + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customers.add(
                    new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                    ));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return customers;
    }

    public boolean addCustomer(Customer customer) {
        boolean success = false;

        try (Connection conn = ConnectionSetup.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO Customer (CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (?,?,?,?,?,?,?)");
            preparedStatement.setInt(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getEmail());

            int result = preparedStatement.executeUpdate();
            success = (result != 0);
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return success;
    }

    public boolean updateCustomer(Customer customer, int id) {
        boolean success = false;
        try (Connection conn = ConnectionSetup.getConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("UPDATE Customer SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?");
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setInt(7, id);
            int result = preparedStatement.executeUpdate();
            success = (result != 0);

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return success;
    }


    public ArrayList<Customer> getCustomerPage(int offset) {
        ArrayList<Customer> customers = new ArrayList<>();
        try (Connection conn = ConnectionSetup.getConnection()){
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId >= ? LIMIT 10");
            preparedStatement.setInt(1, offset);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return customers;
    }

    public ArrayList<CustomerCountry> getAmountOfCustomersInEachCountry() {
        ArrayList<CustomerCountry> customersPerCountry = new ArrayList<>();
        try (Connection conn = ConnectionSetup.getConnection()){
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Country, COUNT(Country) AS Total FROM Customer GROUP BY Country ORDER BY COUNT(Country) DESC");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customersPerCountry.add(new CustomerCountry(
                   resultSet.getString("Country"),
                   resultSet.getInt("Total")
                ));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return customersPerCountry;
    }

    public ArrayList<CustomerSpender> getTotalSpent() {
        ArrayList<CustomerSpender> customerSpenders = new ArrayList<>();
        try (Connection conn = ConnectionSetup.getConnection()){
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, round(SUM(Invoice.Total),2) AS Total_Spent FROM Customer INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId GROUP BY Customer.CustomerId ORDER BY Total_Spent DESC");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerSpenders.add(new CustomerSpender(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getDouble("Total_Spent")
                ));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return customerSpenders;
    }

    public ArrayList<CustomerGenre> getMostPopularGenreByCustomer(int id) {
        ArrayList<CustomerGenre> customerGenres = new ArrayList<>();
        try (Connection conn = ConnectionSetup.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("WITH MostPopularGenre AS (SELECT Genre.Name AS Genre, COUNT(Track.GenreId) AS GenreValue FROM Customer INNER JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId INNER JOIN InvoiceLine ON InvoiceLine.InvoiceId = Invoice.InvoiceId INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId INNER JOIN Genre ON Track.GenreId = Genre.GenreId WHERE Customer.CustomerId = ? GROUP BY Track.GenreId ORDER BY GenreValue DESC) SELECT * FROM MostPopularGenre WHERE GenreValue = (SELECT MAX(GenreValue) FROM MostPopularGenre);");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerGenres.add(new CustomerGenre(
                        resultSet.getString("Genre"),
                        resultSet.getInt("GenreValue")
                ));
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }

        return customerGenres;
    }
}
