package com.assaignment.javaassaignmenttwo.Customer.Controllers;

import com.assaignment.javaassaignmenttwo.Customer.Repository.CustomerRepository;
import com.assaignment.javaassaignmenttwo.Customer.Models.Customer;
import com.assaignment.javaassaignmenttwo.Customer.Models.CustomerCountry;
import com.assaignment.javaassaignmenttwo.Customer.Models.CustomerGenre;
import com.assaignment.javaassaignmenttwo.Customer.Models.CustomerSpender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping(path={"api/customer"})
public class CustomerController {

    private final CustomerRepository customerRepository;

    public CustomerController(@Autowired CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    //All endpoints returns data according to models in Customer/Models folder

    //Returns all the customers in the database according to Models/Customer
    @GetMapping
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }

    //Returns one customer in the database according to Models/Customer. Id is a number > 1
    @GetMapping("{id}")
    public Customer getCustomerById(@PathVariable int id){
        return customerRepository.getCustomerById(id);
    }

    //Returns all customers that contain a part, or all, of the string provided, in their first name, in the database according to Models/Customer.
    @GetMapping("/name/{name}")
    public ArrayList<Customer> getCustomersByName(@PathVariable String name){
        return customerRepository.getCustomersByName(name);
    }

    //Add a customer to the database by providing the fields in Models/Customer in the request body.
    @PostMapping
    public boolean addCustomer(@RequestBody Customer customer) {
        return customerRepository.addCustomer(customer);
    }

    //Returns all customers and their total amount of money spent on music, descending from the highest spender.
    //According to Models/CustomerSpender
    @GetMapping("/spenders")
    public ArrayList<CustomerSpender> getTotalSpent(){
        return customerRepository.getTotalSpent();
    }

    //Returns 10 customers starting with customerId = offset.
    //According to Models/Customer
    @GetMapping("/page/{offset}")
    public ArrayList<Customer> getCustomerPage(@PathVariable int offset){
        return customerRepository.getCustomerPage(offset);
    }

    //Returns number of customers in each country.
    //According to Models/CustomerCountry
    @GetMapping("/countries")
    public ArrayList<CustomerCountry> getAmountOfCustomersInEachCountry(){
        return customerRepository.getAmountOfCustomersInEachCountry();
    }

    //Returns one customer and the genre/genres that they have spent the most money on, in the database according to Models/CustomerGenre.
    // Id is a number > 1
    @GetMapping("{id}/genre")
    public ArrayList<CustomerGenre> getMostPopularGenreByCustomer(@PathVariable int id){
        return customerRepository.getMostPopularGenreByCustomer(id);
    }

    //Update a customers info according to Models/Customer.
    // Id is a number > 1
    @PutMapping("{id}")
    public boolean updateCustomer(@RequestBody Customer customer, @PathVariable int id) {
        return customerRepository.updateCustomer(customer, id);
    }

}
